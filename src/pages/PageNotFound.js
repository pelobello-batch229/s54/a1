
import {Link} from 'react-router-dom'
export default function PageNotFound(){

	return(
		 <div className="text-center">
        <h1>404 Page Not Found</h1>
        Go back to the <Link to="/">homepage</Link>

    	</div>

		)
}