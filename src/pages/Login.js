import {Form, Button} from 'react-bootstrap';
// hook
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext.js'


export default function Login() {
	
	// Allow us to consume the User Context object and its properties
	const {user, setUser} = useContext(UserContext);



	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// State to dertermine whether submit button is enabled or not
	const[isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password)

	//validation to enable submit button when all fields are populated and both password match
	useEffect(() => {
		
		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])
	//dependency array



	function userLogin(event) {
		// Prevent pag to redirection via form submission
		event.preventDefault();

		// Set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem('propertyName', value)
		*/
		// setItem saving to the localstorage

		localStorage.setItem('email', email);

		setUser({
			// getItem getting the item in the localstorage
			email: localStorage.getItem('email')
		})

		setEmail('');
		setPassword('');
		alert(`${email}You are now logged in`);
	}

	return(
		
		(user.email !== null)?
			<Navigate to="/courses"/>
		:


		    <Form onSubmit={event=> userLogin(event)} >
		    	<h2>Login</h2>
		      <Form.Group className="mb-3" controlid="userEmail">
		        <Form.Label>Email</Form.Label>
			        <Form.Control 
			        type="email" 
			        placeholder="Enter email"  
			        value={email} 
			        onChange={event => setEmail(event.target.value)} 
			        required
			        />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlid="Password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required/>
		      </Form.Group>

		     {
		     	isActive?	
		     	<Button variant="success" type="submit" controlid="submitBtn" >Login</Button>	
		     	: 
		     	<Button variant="success" type="submit" controlid="submitBtn" disabled>Login</Button>	
		     }
		      

		    </Form>
		)
}