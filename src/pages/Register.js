import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js'
import {Navigate} from 'react-router-dom'
// use useEffect conditional limit // Like a function


export default function Register() {

	// State Hooks -> store values of the inpute fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const[isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1)
	console.log(password2)

	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	})

	// Function to simulate user registration

	function registerUser(event) {
		// prevents page redirection via form submission
		event.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');

		alert("Thank you for registering");
	}
	const {user} = useContext(UserContext);

	return(

		(user.email !== null)?
			<Navigate to="/courses"/>
		:
		    <Form onSubmit={event=> registerUser(event)} >
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email"  value={email} onChange={event => setEmail(event.target.value)} required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="Password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password1} onChange={event => setPassword1(event.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="Password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={event => setPassword2(event.target.value)} required />
		      </Form.Group>

		     {/*Conditional Rendering -> If active button is clickable -> inactive button is not clickable*/}
		     {
		     	(isActive)?	
		     	<Button variant="primary" type="submit" controlId="submitBtn" >Register</Button>	

		     	: 
		     	<Button variant="primary" type="submit" controlId="submitBtn" disabled>Register</Button>	
		     }
		      

		    </Form>
		)
}